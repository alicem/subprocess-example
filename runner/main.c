#include <glib-2.0/glib.h>
#include <glib-unix.h>
#include <gio/gunixconnection.h>

#ifdef __linux__
#include <sys/prctl.h>
#endif

#ifdef __FreeBSD__
#include <sys/procctl.h>
#endif

#include "ipc-runner-impl.h"

// Adapted from GNOME Builder's gnome-builder-git.c
gint
main (gint    argc,
      gchar **argv)
{
  g_autoptr(GError) error = NULL;
  g_autoptr(GMainLoop) loop = NULL;
  g_autoptr(GSocket) socket = NULL;
  g_autoptr(GIOStream) stream = NULL;
  g_autoptr(GDBusConnection) connection = NULL;
  g_autoptr(IpcRunnerImpl) runner = NULL;
  g_autofree gchar *guid = NULL;

  g_set_prgname ("retro-gtk-runner");
  g_set_application_name ("retro-gtk-runner");

#ifdef __linux__
  prctl (PR_SET_PDEATHSIG, SIGTERM);
#elif defined(__FreeBSD__)
  procctl (P_PID, 0, PROC_PDEATHSIG_CTL, &(int){ SIGTERM });
#else
#error "Please submit a patch to support parent-death signal on your OS"
#endif

  loop = g_main_loop_new (NULL, FALSE);

  g_debug ("Starting subprocess");

  // 3 file descriptor is passed from the parent process
  if (!g_unix_set_fd_nonblocking (3, TRUE, &error))
    goto error;

  socket = g_socket_new_from_fd (3, &error);
  stream = G_IO_STREAM (g_socket_connection_factory_create_connection (socket));

  g_assert (G_IS_UNIX_CONNECTION (stream));

  guid = g_dbus_generate_guid ();
  connection = g_dbus_connection_new_sync (stream, guid,
                                           G_DBUS_CONNECTION_FLAGS_DELAY_MESSAGE_PROCESSING |
                                           G_DBUS_CONNECTION_FLAGS_AUTHENTICATION_SERVER,
                                           NULL, NULL, &error);
  if (!connection)
    goto error;

  g_debug ("Connected");

  g_dbus_connection_set_exit_on_close (connection, FALSE);
  g_signal_connect_swapped (connection, "closed", G_CALLBACK (g_main_loop_quit), loop);

  runner = ipc_runner_impl_new ();

  if (!g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (runner),
                                         connection,
                                         "/org/example/App/Runner",
                                         &error))
    goto error;

  ipc_runner_impl_start (runner);
  g_dbus_connection_start_message_processing (connection);

  g_debug ("Running main loop");

  g_main_loop_run (loop);

  g_debug ("Exiting subprocess");

  return EXIT_SUCCESS;

error:
  if (error)
    g_critical ("Couldn't initialize subprocess: %s", error->message);

  return EXIT_FAILURE;
}

