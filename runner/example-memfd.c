#include "example-memfd.h"

#include <glib/gstdio.h>

// Copied from sysprof's sysprof_memfd_create()
gint
example_memfd_create (const gchar *name)
{
#ifdef __NR_memfd_create
  return syscall (__NR_memfd_create, name, 0);
#else
  gchar *name_used = NULL;
  gint fd;

  /*
   * TODO: It would be nice to ensure tmpfs
   *
   * It is not strictly required that the preferred temporary directory
   * will be mounted as tmpfs. We should look through the mounts and ensure
   * that the tmpfile we open is on tmpfs so that we get anonymous backed
   * pages after unlinking.
   */
  fd = g_file_open_tmp (NULL, &name_used, NULL);

  if (name_used != NULL) {
    g_unlink (name_used);
    g_free (name_used);
  }

  return fd;
#endif
}
