#include "ipc-runner-impl.h"

#include "example-framebuffer.h"
#include <gio/gunixfdlist.h>
#include "math.h"

typedef struct {
  guint32 r: 8;
  guint32 g: 8;
  guint32 b: 8;
  guint32 a: 8;
} rgba8888;

struct _IpcRunnerImpl
{
  IpcRunnerSkeleton parent_instance;
  gint time;
  gboolean paused;
  guint timeout_id;
  ExampleFramebuffer *buffers[2];
};

static void ipc_runner_iface_init (IpcRunnerIface *iface);

G_DEFINE_TYPE_WITH_CODE (IpcRunnerImpl, ipc_runner_impl, IPC_TYPE_RUNNER_SKELETON,
                         G_IMPLEMENT_INTERFACE (IPC_TYPE_RUNNER, ipc_runner_iface_init))

static gboolean
ipc_runner_impl_handle_pause (IpcRunner             *runner,
                              GDBusMethodInvocation *invocation)
{
  IpcRunnerImpl *self;

  g_assert (IPC_IS_RUNNER (runner));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  self = IPC_RUNNER_IMPL (runner);

  self->paused = TRUE;

  ipc_runner_complete_pause (runner, invocation);
  return TRUE;
}

static gboolean
ipc_runner_impl_handle_unpause (IpcRunner             *runner,
                                GDBusMethodInvocation *invocation)
{
  IpcRunnerImpl *self;

  g_assert (IPC_IS_RUNNER (runner));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  self = IPC_RUNNER_IMPL (runner);

  self->paused = FALSE;

  ipc_runner_complete_unpause (runner, invocation, self->time);
  return TRUE;
}

static void
create_image (IpcRunnerImpl      *self,
              ExampleFramebuffer *buf) {
  rgba8888 *pixels;
  gint x, y;
  gsize w, h;

  g_object_get (buf, "width", &w, "height", &h, "buffer", &pixels, NULL);

  for (x = 0; x < w; x++) {
    gdouble cos1 = cos ((gdouble) x / w * G_PI + self->time / 7.0);
    gdouble cos2 = sin ((gdouble) x / w * G_PI + self->time / 13.0);

    for (y = 0; y < h; y++) {
      rgba8888 *p;

      p = pixels + y * w + x;
      p->r = floor ((cos1 + 1) * 127.5);
      p->g = floor ((cos2 + 1) * 127.5);
      p->a = 50;
    }
  }

  for (y = 0; y < h; y++) {
    gdouble cos3 = cos ((gdouble) y / h / 2 * G_PI + self->time / 19.0);

    for (x = 0; x < w; x++) {
      rgba8888 *p;

      p = pixels + y * w + x;
      p->b = floor ((cos3 + 1) * 127.5);
    }
  }
}

static void
swap_buffers (IpcRunnerImpl *self)
{
  ExampleFramebuffer *tmp;

  tmp = self->buffers[0];
  self->buffers[0] = self->buffers[1];
  self->buffers[1] = tmp;
}

static gboolean
ipc_runner_impl_handle_get_frame (IpcRunner             *runner,
                                  GDBusMethodInvocation *invocation,
                                  gsize                  width,
                                  gsize                  height)
{
  IpcRunnerImpl *self;
  GDBusConnection *connection;
  g_autoptr(GError) error = NULL;
  g_autoptr(GUnixFDList) out_fd_list = NULL;
  gint fd, handle;

  g_assert (IPC_IS_RUNNER (runner));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  self = IPC_RUNNER_IMPL (runner);

  if (!self->buffers[0])
    self->buffers[0] = example_framebuffer_new (width, height, sizeof (rgba8888));

  g_object_set (self->buffers[0], "width", width, "height", height, NULL);
  g_object_get (self->buffers[0], "fd", &fd, NULL);

  lseek (fd, 0, SEEK_SET);
  create_image (self, self->buffers[0]);

  out_fd_list = g_unix_fd_list_new ();
  handle = g_unix_fd_list_append (out_fd_list, fd, &error);

  if (handle == -1) {
    g_warning ("Failed to create memfd for peer: %s", error->message);
    return TRUE;
  }

  connection = g_dbus_method_invocation_get_connection (invocation);
  if ((g_dbus_connection_get_capabilities (connection) & G_DBUS_CAPABILITY_FLAGS_UNIX_FD_PASSING) == 0) {
    g_dbus_method_invocation_return_error (g_steal_pointer (&invocation),
                                           G_DBUS_ERROR,
                                           G_DBUS_ERROR_NOT_SUPPORTED,
                                           "Couldn't return frame file descriptor, the connection must support FD passing");
    return TRUE;
  }

  g_dbus_method_invocation_return_value_with_unix_fd_list (g_steal_pointer (&invocation),
                                                           g_variant_new ("(th)", width * sizeof (rgba8888), handle),
                                                           out_fd_list);

  swap_buffers (self);

  return TRUE;
}

static void
ipc_runner_impl_finalize (GObject *object)
{
  IpcRunnerImpl *self = (IpcRunnerImpl *)object;

  g_clear_object (&self->buffers[0]);
  g_clear_object (&self->buffers[1]);

  g_source_remove (self->timeout_id);
  self->timeout_id = 0;

  G_OBJECT_CLASS (ipc_runner_impl_parent_class)->finalize (object);
}

static void
ipc_runner_iface_init (IpcRunnerIface *iface)
{
  iface->handle_pause = ipc_runner_impl_handle_pause;
  iface->handle_unpause = ipc_runner_impl_handle_unpause;
  iface->handle_get_frame = ipc_runner_impl_handle_get_frame;
}

static void
ipc_runner_impl_class_init (IpcRunnerImplClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = ipc_runner_impl_finalize;
}

static void
ipc_runner_impl_init (IpcRunnerImpl *self)
{
  self->paused = FALSE;
  self->time = 0;
}

IpcRunnerImpl *
ipc_runner_impl_new (void)
{
  return g_object_new (IPC_TYPE_RUNNER_IMPL, NULL);
}

static gboolean
timeout_cb (IpcRunnerImpl *self)
{
  if (!self->paused) {
    self->time++;
    ipc_runner_emit_tick (IPC_RUNNER (self), self->time);
  }

  return G_SOURCE_CONTINUE;
}

void
ipc_runner_impl_start (IpcRunnerImpl *self)
{
  g_return_if_fail (IPC_RUNNER_IMPL (self));
  g_return_if_fail (self->timeout_id == 0);

  ipc_runner_emit_tick (IPC_RUNNER (self), 0);
  self->timeout_id = g_timeout_add_full (G_PRIORITY_DEFAULT_IDLE, 16,
                                         (GSourceFunc) timeout_cb, self, NULL);
}

