#pragma once

#include "ipc-runner.h"

G_BEGIN_DECLS

#define IPC_TYPE_RUNNER_IMPL (ipc_runner_impl_get_type())

G_DECLARE_FINAL_TYPE (IpcRunnerImpl, ipc_runner_impl, IPC, RUNNER_IMPL, IpcRunnerSkeleton)

IpcRunnerImpl *ipc_runner_impl_new (void);

void ipc_runner_impl_start (IpcRunnerImpl *self);

G_END_DECLS
