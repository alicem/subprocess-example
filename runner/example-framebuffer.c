#include "example-framebuffer.h"

#include "example-memfd.h"
#include <sys/mman.h>
#include <unistd.h>

struct _ExampleFramebuffer
{
  GObject parent_instance;

  gsize width;
  gsize height;
  gsize depth;

  gint fd;
  gpointer buffer;
  gsize size;
};

G_DEFINE_TYPE (ExampleFramebuffer, example_framebuffer, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_WIDTH,
  PROP_HEIGHT,
  PROP_DEPTH,
  PROP_FD,
  PROP_BUFFER,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
size_changed (ExampleFramebuffer *self)
{
  gsize size;

  size = self->width * self->height * self->depth;

  if (size == self->size)
    return;

  if (self->buffer)
    munmap (self->buffer, self->size);

  self->size = size;
  ftruncate (self->fd, size);

  self->buffer = mmap (NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED,
                       self->fd, 0);
}

static void
example_framebuffer_finalize (GObject *object)
{
  ExampleFramebuffer *self = (ExampleFramebuffer *)object;

  close (self->fd);

  if (self->buffer)
    munmap (self->buffer, self->size);

  G_OBJECT_CLASS (example_framebuffer_parent_class)->finalize (object);
}

static void
example_framebuffer_get_property (GObject    *object,
                                  guint       prop_id,
                                  GValue     *value,
                                  GParamSpec *pspec)
{
  ExampleFramebuffer *self = EXAMPLE_FRAMEBUFFER (object);

  switch (prop_id) {
  case PROP_WIDTH:
    g_value_set_uint64 (value, self->width);
    break;
  case PROP_HEIGHT:
    g_value_set_uint64 (value, self->height);
    break;
  case PROP_DEPTH:
    g_value_set_uint64 (value, self->depth);
    break;
  case PROP_FD:
    g_value_set_int (value, self->fd);
    break;
  case PROP_BUFFER:
    g_value_set_pointer (value, self->buffer);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
example_framebuffer_set_property (GObject      *object,
                                  guint         prop_id,
                                  const GValue *value,
                                  GParamSpec   *pspec)
{
  ExampleFramebuffer *self = EXAMPLE_FRAMEBUFFER (object);

  switch (prop_id) {
  case PROP_WIDTH:
    self->width = g_value_get_uint64 (value);
    size_changed (self);
    break;
  case PROP_HEIGHT:
    self->height = g_value_get_uint64 (value);
    size_changed (self);
    break;
  case PROP_DEPTH:
    self->depth = g_value_get_uint64 (value);
    size_changed (self);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
example_framebuffer_class_init (ExampleFramebufferClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = example_framebuffer_finalize;
  object_class->get_property = example_framebuffer_get_property;
  object_class->set_property = example_framebuffer_set_property;

  properties [PROP_WIDTH] =
    g_param_spec_uint64 ("width",
                         "Width",
                         "Width",
                         1, G_MAXUINT64, 1,
                         (G_PARAM_READWRITE |
                          G_PARAM_CONSTRUCT |
                          G_PARAM_STATIC_STRINGS));

  properties [PROP_HEIGHT] =
    g_param_spec_uint64 ("height",
                         "Height",
                         "Height",
                         1, G_MAXUINT64, 1,
                         (G_PARAM_READWRITE |
                          G_PARAM_CONSTRUCT |
                          G_PARAM_STATIC_STRINGS));

  properties [PROP_DEPTH] =
    g_param_spec_uint64 ("depth",
                         "Depth",
                         "Pixel depth",
                         1, G_MAXUINT64, 1,
                         (G_PARAM_READWRITE |
                          G_PARAM_CONSTRUCT_ONLY |
                          G_PARAM_STATIC_STRINGS));

  properties [PROP_FD] =
    g_param_spec_int ("fd",
                      "File descriptor",
                      "File descriptor",
                      -1, G_MAXINT, -1,
                      (G_PARAM_READABLE |
                       G_PARAM_STATIC_STRINGS));

  properties [PROP_BUFFER] =
    g_param_spec_pointer ("buffer",
                          "Buffer",
                          "Buffer",
                          (G_PARAM_READABLE |
                           G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
example_framebuffer_init (ExampleFramebuffer *self)
{
  self->width = 1;
  self->height = 1;
  self->depth = 1;
  self->size = 0;
  self->fd = example_memfd_create ("[framebuffer]");
}

ExampleFramebuffer *
example_framebuffer_new (gsize width,
                         gsize height,
                         gsize depth)
{
  return g_object_new (EXAMPLE_TYPE_FRAMEBUFFER,
                       "width", width,
                       "height", height,
                       "depth", depth,
                       NULL);
}

