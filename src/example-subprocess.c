#include "example-subprocess.h"

#include <glib-unix.h>
#include <gio/gunixconnection.h>

#include <sys/socket.h>

struct _ExampleSubprocess
{
  GObject parent_instance;

  GDBusConnection *connection;
};

enum {
  SIGNAL_EXIT,
  N_SIGNALS,
};

static guint signals [N_SIGNALS];

G_DEFINE_TYPE (ExampleSubprocess, example_subprocess, G_TYPE_OBJECT)

static void
example_subprocess_dispose (GObject *object)
{
  ExampleSubprocess *self = (ExampleSubprocess *)object;

  if (self->connection) {
    g_dbus_connection_close_sync (self->connection, NULL, NULL);
    g_clear_object (&self->connection);
  }

  G_OBJECT_CLASS (example_subprocess_parent_class)->dispose (object);
}

static void
example_subprocess_class_init (ExampleSubprocessClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = example_subprocess_dispose;

  signals [SIGNAL_EXIT] =
    g_signal_new ("exit",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0, NULL, NULL, NULL,
                  G_TYPE_NONE,
                  2, G_TYPE_BOOLEAN, G_TYPE_INT);
}

static void
example_subprocess_init (ExampleSubprocess *self)
{
}

ExampleSubprocess *
example_subprocess_new (void)
{
  return g_object_new (EXAMPLE_TYPE_SUBPROCESS, NULL);
}

static void
wait_check_cb (GSubprocess       *process,
               GAsyncResult      *result,
               ExampleSubprocess *self)
{
  gboolean success;
  g_autoptr(GError) error = NULL;

  success = g_subprocess_wait_check_finish (process, result, &error);
  if (error)
    g_warning ("Error when executing g_subprocess_wait_check(): %s", error->message);

  g_signal_emit (self, signals[SIGNAL_EXIT], 0, success,
                 g_subprocess_get_status (process));

  g_clear_object (&self->connection);
}

// Adapted from GNOME Builder's gbp-git-client.c
IpcRunner *
example_subprocess_start (ExampleSubprocess  *self,
                          GError            **error)
{
  GSocket *socket = NULL;
  g_autoptr(GSocketConnection) connection = NULL;
  g_autoptr(GSubprocessLauncher) launcher = NULL;
  g_autoptr(GSubprocess) process = NULL;
  gint status, sv[2];
  IpcRunner *proxy;

  g_return_val_if_fail (EXAMPLE_IS_SUBPROCESS (self), NULL);
  g_return_val_if_fail (!G_IS_DBUS_CONNECTION (self->connection), NULL);

  status = socketpair (PF_UNIX, SOCK_STREAM, 0, sv);
  if (status != 0)
    return NULL;

  if (!g_unix_set_fd_nonblocking (sv[0], TRUE, error) ||
      !g_unix_set_fd_nonblocking (sv[1], TRUE, error))
    return NULL;

  launcher = g_subprocess_launcher_new (G_SUBPROCESS_FLAGS_NONE);
  g_subprocess_launcher_take_fd (launcher, sv[1], 3);

  socket = g_socket_new_from_fd (sv[0], error);
  if (!socket)
    return NULL;

  connection = g_socket_connection_factory_create_connection (socket);
  if (!connection)
    return NULL;

  g_assert (G_IS_UNIX_CONNECTION (connection));

  g_object_unref (socket);

  process = g_subprocess_launcher_spawn (launcher, error,
                                         "/app/libexec/subprocess-runner",
                                         NULL);
  if (!process)
    return NULL;

  self->connection = g_dbus_connection_new_sync (G_IO_STREAM (connection), NULL,
                                                 G_DBUS_CONNECTION_FLAGS_DELAY_MESSAGE_PROCESSING |
                                                 G_DBUS_CONNECTION_FLAGS_AUTHENTICATION_CLIENT,
                                                 NULL, NULL, error);
  if (!self->connection)
    return NULL;

  g_dbus_connection_set_exit_on_close (self->connection, FALSE);
  g_dbus_connection_start_message_processing (self->connection);

  proxy = ipc_runner_proxy_new_sync (self->connection,
                                     G_DBUS_PROXY_FLAGS_DO_NOT_LOAD_PROPERTIES,
                                     NULL, "/org/example/App/Runner", NULL, NULL);

  g_subprocess_wait_check_async (process, NULL, (GAsyncReadyCallback) wait_check_cb, self);

  return g_object_ref (proxy);
}

void
example_subprocess_stop (ExampleSubprocess  *self,
                         GError            **error)
{
  g_return_if_fail (EXAMPLE_IS_SUBPROCESS (self));
  g_return_if_fail (G_IS_DBUS_CONNECTION (self->connection));

  g_dbus_connection_close_sync (self->connection, NULL, error);
}

